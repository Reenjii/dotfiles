#!/bin/sh

DOTFILES="$HOME/dotfiles"

GIT=git

OH_MY_ZSH="$HOME/.oh-my-zsh"
VUNDLE="$HOME/.vim/bundle/vundle"

PWD=`pwd`

function checkgit {
	LIB=$1
	DIR=$2
	REPO=$3
	echo "[-] Checking git repo $DIR"
	if [ -e $DIR ]
	then
		echo "[OK] Folder $DIR already exists"
		echo "Performing git pull ?"
		select yn in "Yes" "No"; do
			case $yn in
				Yes ) cd $DIR ; $GIT pull ; cd $PWD ; break;;
				No ) break;;
			esac
		done
	else
		echo "[-] Folder $DIR not found, installing $LIB"
		$GIT clone $REPO $DIR
	fi
}

checkgit "oh-my-zsh" $OH_MY_ZSH "git://github.com/robbyrussell/oh-my-zsh.git"
checkgit "Vundle" $VUNDLE "https://github.com/gmarik/vundle.git"

function checklink {
	LINK=$1
	TARGET=$2
	echo "[-] Checking file $LINK"
	if [ -e $LINK ]
	then
		if [ -L $LINK ]
		then
			LINKTARGET=`readlink $LINK`
			if [ $LINKTARGET != $TARGET ]
			then
				echo "[KO] $LINK is a symbolic link to $LINKTARGET"
				echo "[-] It should link to $TARGET instead"
				echo "[?] Do you want to correct it ?"
				select yn in "Yes" "No"; do
					case $yn in
						Yes ) rm -v -f $LINK ; ln -v -s $TARGET $LINK ; break;;
						No ) break;;
					esac
				done
			else
				echo "[OK] $LINK is a symbolic link to $LINKTARGET"
			fi
		else
			echo "[KO] $LINK already exists but is not a symbolic link"
			echo "[!] You must remove this folder to install $TARGET config files"
		fi
	else
		echo "[-] $LINK not found"
		echo "[-] Installing $TARGET config files"
		ln -v -s $TARGET $LINK
	fi
}

# Symbolic links for dirs
for DIR in .shell
do
	checklink $HOME/$DIR $DOTFILES/$DIR
done

# Symbolic links for files in .shell
for FILE in bashrc zshrc
do
	checklink $HOME/.$FILE $DOTFILES/.shell/$FILE
done

# Links for files in .config/{NAME}/{NAME}rc
for FILE in screen htop
do
	checklink "$HOME/.${FILE}rc" "$DOTFILES/.config/$FILE/${FILE}rc"
done

# Vim
checklink $HOME/.vimrc $DOTFILES/.vim/vimrc/vimrc
checklink $HOME/.vim/vimrc $DOTFILES/.vim/vimrc
mkdir $HOME/.vim/undofiles 2> /dev/null
echo "Do you want to install vim Vundle bundles ?"
select yn in "Yes" "No"; do
	case $yn in
		Yes ) vim +BundleInstall +qall ; break;;
		No ) break;;
	esac
done
