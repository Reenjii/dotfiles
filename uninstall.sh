#!/bin/sh

DOTFILES="$HOME/dotfiles"

GIT=git

OH_MY_ZSH="$HOME/.oh-my-zsh"
VUNDLE="$HOME/.vim/bundle/vundle"

PWD=`pwd`

function rmgit {
	DIR=$1
	echo "[-] Remove git repo $DIR"
	if [ -e $DIR ]
	then
		echo "[-] Folder $DIR exists"
		echo "[?] Delet folder $DIR ?"
		select yn in "Yes" "No"; do
			case $yn in
				Yes ) rm -rf $DIR && echo "[OK] Deleted $DIR"; break;;
				No ) break;;
			esac
		done
	else
		echo "[OK] Folder $DIR not found"
	fi
}

rmgit $OH_MY_ZSH
rmgit $VUNDLE

function rmlink {
	LINK=$1
	echo "[-] Remove link $LINK"
	if [ -e $LINK ]
	then
		if [ -L $LINK ]
		then
			LINKTARGET=`readlink $LINK`
			echo "[-] $LINK is a symbolic link to $LINKTARGET"
			echo "[?] Do you want to delete $LINK ?"
				select yn in "Yes" "No"; do
					case $yn in
						Yes ) rm -v -f $LINK && echo "[OK] Deleted $LINK"; break;;
						No ) break;;
					esac
				done
		else
			echo "[KO] $LINK already exists but is not a symbolic link"
		fi
	else
		echo "[OK] $LINK not found"
	fi
}

# Symbolic links for dirs
for DIR in .shell
do
	rmlink $HOME/$DIR
done

# Symbolic links for files in .shell
for FILE in bashrc zshrc
do
	rmlink $HOME/.$FILE
done

# Links for files in .config/{NAME}/{NAME}rc
for FILE in screen htop
do
	rmlink "$HOME/.${FILE}rc"
done

# Vim
rmlink $HOME/.vimrc
rmlink $HOME/.vim/vimrc
if [ -e $HOME/.vim/bundle ]
then
	echo "Do you want to uninstall vim Vundle bundles (remove $HOME/.vim/bundle) ?"
	select yn in "Yes" "No"; do
		case $yn in
			Yes ) rm -rf $HOME/.vim/bundle ; break;;
			No ) echo "Did not change anything"; break;;
		esac
	done
fi
